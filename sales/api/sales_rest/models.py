from django.db import models

# Create your models here.
class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin
    

class SalePerson(models.Model):
    name = models.CharField(max_length=200, unique=True)
    employer_id = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.name 
    
    class Meta:
        ordering = ('name',)


class Customer(models.Model):
    name = models.CharField(max_length=200, unique=True)
    email = models.EmailField()
    phone_number = models.PositiveBigIntegerField()

    def __str__(self):
       return self.name

    class Meta:
        ordering = ('name',)


class AutoSale(models.Model):
    sales_person = models.ForeignKey(
        SalePerson,
        related_name='autosales',
        on_delete=models.CASCADE,
    )
    potential_customer = models.ForeignKey(
        Customer,
        related_name='autosales',
        on_delete=models.CASCADE,
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name='autosales',
        on_delete=models.CASCADE,
    )
    sale_price = models.PositiveIntegerField()


    def __str__(self):
        return str(self.automobile.vin) + ' ' + str(self.potential_customer.name)