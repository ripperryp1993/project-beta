from django.urls import path
from .views import api_customer_list, api_delete_customer,api_sale_person_list, api_delete_sale_person, api_sales_list, api_delete_auto_sales


urlpatterns = [
    path('customers/', api_customer_list, name="api_customers"),
    path('customers/<int:pk>/', api_delete_customer, name="delete_customers"),
    path('sales_person/', api_sale_person_list, name="api_sales_person"),
    path('sales_person/<int:pk>/', api_delete_sale_person, name="delete_sales_person"),
    path('autosales/', api_sales_list, name="api_autosales"),
    path('autosales/<int:pk>/', api_delete_auto_sales, name="delete_autosales"),
]
