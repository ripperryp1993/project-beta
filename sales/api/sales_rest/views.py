from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import SalePerson, AutomobileVO, AutoSale, Customer
from common.json import ModelEncoder
import json
# Create your views here.

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ['import_href', 'color', 'year', 'vin']


class SalePersonEncoder(ModelEncoder):
    model = SalePerson
    properties = ['name', 'employer_id', 'id']


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ['name', 'email', 'phone_number', 'id']


class AutoSaleEncoder(ModelEncoder):
    model = AutoSale
    properties = ['sale_price', 'id', 'automobile','sales_person', 'potential_customer']
    encoders = {
         'automobile': AutomobileVOEncoder(),
         'sales_person': SalePersonEncoder(),
         'potential_customer': CustomerEncoder()
     }

    # def get_extra_data(self, o):
    #     return {'automobile': o.automobile.vin, 'sales_person': o.sales_person.name, 'potential_customer': o.potential_customer.name}



@require_http_methods(['GET', 'POST'])
def api_sale_person_list(request):
    if request.method == 'GET':
        sale_person = SalePerson.objects.all()
        return JsonResponse(
        {'sales_person':sale_person},
        encoder=SalePersonEncoder,
        safe=False
      )
    else:
        content = json.loads(request.body)
        sales_person = SalePerson.objects.create(**content)
        return JsonResponse(
          sales_person,
          encoder=SalePersonEncoder,
          safe=False
        )



@require_http_methods(["DELETE"])
def api_delete_sale_person(request, pk):
    try:
        sale_person = SalePerson.objects.get(id=pk)
        sale_person.delete()
        return JsonResponse(
          sale_person,
          encoder=SalePersonEncoder,
          safe=False,
        )
    except SalePerson.DoesNotExist:
        return JsonResponse({"message": "Does not exist"})



@require_http_methods(['GET', 'POST'])
def api_customer_list(request):
    if request.method == 'GET':
        customer = Customer.objects.all()
        return JsonResponse(
        {'potential_customer':customer},
        encoder=CustomerEncoder,
        safe=False
      )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
          customer,
          encoder=CustomerEncoder,
          safe=False
        )



@require_http_methods(["DELETE"])
def api_delete_customer(request, pk):
    try:
        customer = Customer.objects.get(id=pk)
        customer.delete()
        return JsonResponse(
          customer,
          encoder=CustomerEncoder,
          safe=False,
        )
    except Customer.DoesNotExist:
        return JsonResponse({"message": "Does not exist"})



@require_http_methods(["GET", "POST"])
def api_sales_list(request):
    if request.method == "GET":
      sales = AutoSale.objects.all()
      return JsonResponse(
        {'sales':sales},
        encoder=AutoSaleEncoder,
        safe=False
      )
    else: #POST
        content = json.loads(request.body)
        print('content', content)
        try:
            automobile = AutomobileVO.objects.get(import_href=content["automobile"])
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile href"},
                status=400,
            )
        try:
            sale_person = SalePerson.objects.get(name=content["sales_person"])
            content["sales_person"] = sale_person
        except SalePerson.DoesNotExist:
            return JsonResponse(
                {"message": "Sale person does not exist"},
                status=400,
            )
        try:
            customer = Customer.objects.get(name=content["potential_customer"])
            content["potential_customer"] = customer
            print('customer', customer)
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400,
            )

        auto_sale = AutoSale.objects.create(**content)
        return JsonResponse(
          auto_sale,
          encoder=AutoSaleEncoder,
          safe=False
        )

@require_http_methods(["DELETE"])
def api_delete_auto_sales(request, pk):
    try:
        auto_sale = AutoSale.objects.get(id=pk)
        auto_sale.delete()
        return JsonResponse(
          auto_sale,
          encoder=AutoSaleEncoder,
          safe=False,
        )
    except AutoSale.DoesNotExist:
        return JsonResponse({"message": "Does not exist"})