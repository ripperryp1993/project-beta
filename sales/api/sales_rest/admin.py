from django.contrib import admin
from .models import AutomobileVO, AutoSale, Customer
# Register your models here.

class AutoSaleAdmin(admin.ModelAdmin):
    pass

class AutomobileVOAdmin(admin.ModelAdmin):
    pass

class CustomerAdmin(admin.ModelAdmin):
    pass

admin.site.register(AutomobileVO, AutomobileVOAdmin)
admin.site.register(AutoSale, AutoSaleAdmin)
admin.site.register(Customer, CustomerAdmin)