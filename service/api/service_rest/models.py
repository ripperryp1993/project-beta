from django.db import models
from django.urls import reverse

class TechnicianModel(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveIntegerField()

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})

class AutomotiveVOModel(models.Model):
    vin = models.CharField(max_length=100, unique=True)
    href = models.CharField(max_length=200, unique=True)

class ServiceModel(models.Model):
    automobile_vin = models.CharField(max_length=100)
    customer = models.CharField(max_length=100)
    date_time = models.DateTimeField(null=True)
    technician = models.ForeignKey( TechnicianModel, related_name="technician", on_delete=models.CASCADE)
    reason = models.CharField(max_length=100)
    vip = models.BooleanField(default=False)
    completion = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("api_service", kwargs={"pk": self.id})
