import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import TechnicianModel, ServiceModel, AutomotiveVOModel
from common.json import ModelEncoder

class AutomotiveEncoder(ModelEncoder):
    model = AutomotiveVOModel
    properties = ["vin"]

class TechnicianEncoder(ModelEncoder):
    model = TechnicianModel
    properties = ["id", "name", "employee_number"]

class ServiceDetailEncoder(ModelEncoder):
    model = ServiceModel
    properties = [
        "automobile_vin",
        "customer",
        "technician",
        "date_time",
        "reason",
        "vip",
    ]
    encoders = {
        "technician": TechnicianEncoder()
        }

class ServiceListEncoder(ModelEncoder):
    model = ServiceModel
    properties = [
        "id",
        "automobile_vin",
        "customer",
        "technician",
        "date_time",
        "reason",
        "vip",
        "completion",
    ]
    encoders = {
        "technician": TechnicianEncoder()
        }

@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = TechnicianModel.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder, safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            technician = TechnicianModel.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except TechnicianModel.DoesNotExist:
            response = JsonResponse(
                {"message": "Could not add technician"}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET", "DELETE"])
def api_technician(request, pk):
    if request.method == "GET":
        try:
            technician = TechnicianModel.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder, safe=False
            )
        except TechnicianModel.DoesNotExist:
            return JsonResponse(
                {"message": "Could not find technician"}, status=404
            )
    else:
        count, _ = TechnicianModel.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
        

@require_http_methods(["GET", "POST"])
def api_service_appts(request):
    if request.method == "GET":
        appointments = ServiceModel.objects.all()
        return JsonResponse(
            {"appointments": appointments}, encoder=ServiceListEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            technician_id = content["technician"]
            technician = TechnicianModel.objects.get(id=technician_id)
            content["technician"] = technician
        except TechnicianModel.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=404)
        vin = content["automobile_vin"]
        try:
            foo = AutomotiveVOModel.objects.get(vin=str(vin))
            print(foo)
            content["vip"] = True
        except AutomotiveVOModel.DoesNotExist:
            content["vip"] = False

        service = ServiceModel.objects.create(**content)
        return JsonResponse(
            service,
            encoder=ServiceDetailEncoder,
            safe=False,
        )


@require_http_methods(["PUT", "DELETE"])
def api_service_appt(request, pk):
    if request.method == "DELETE":
        count, _ = ServiceModel.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        ServiceModel.objects.filter(id=pk).update(**content)
        appt = ServiceModel.objects.get(id=pk)
        return JsonResponse(
            appt,
            encoder=ServiceDetailEncoder,
            safe=False,
        )

  

