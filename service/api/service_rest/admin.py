from django.contrib import admin
from .models import AutomotiveVOModel, TechnicianModel, ServiceModel

class AutomotiveVOModelAdmin(admin.ModelAdmin):
    pass

class TechnicianModelAdmin(admin.ModelAdmin):
    pass

class ServiceModelAdmin(admin.ModelAdmin):
    pass

admin.site.register(AutomotiveVOModel, AutomotiveVOModelAdmin)
admin.site.register(TechnicianModel, TechnicianModelAdmin)
admin.site.register(ServiceModel, ServiceModelAdmin)

