import React from "react";
import { Link } from 'react-router-dom'

function ManufacturersList(props) {

    return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Manufacturers</th>
            </tr>
          </thead>
          <tbody>
            {props.manufacturers.map(manufacturer => {
              return (
                <tr key={manufacturer.href}>
                  <td>{manufacturer.name}</td>
              </tr>
              );
            })}
            <tr>
              <td>
                <Link to='/manufacturers/new'>Add Manufacturer</Link>
              </td>
            </tr>
            <tr>
              <td>
                <Link to='/manufacturers/delete'>Delete Manufacturer</Link>
              </td>
            </tr>
          </tbody>
        </table>
    );
}

export default ManufacturersList; 