import React from "react";
import { Link } from "react-router-dom";

function CustomerList(props) {

    return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Potential Customer</th>
              <th>Email</th>
              <th>Phone number</th>
            </tr>
          </thead>
          <tbody>
            {props.customers.map(customer => {
              return (
                <tr key={customer.id}>
                  <td>{customer.name}</td>
                  <td>{customer.email}</td>
                  <td>{customer.phone_number}</td>
              </tr>
              );
            })}
            <tr>
                <td>
                    <Link to='/customer/new'>Add a customer</Link>
                </td>
                <td>
                    <Link to='/customer/delete'>Delete a customer</Link>
                </td>
            </tr>
          </tbody>
        </table>
    );
}

export default CustomerList; 