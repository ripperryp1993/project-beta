import React from "react";

class ServiceHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            search_vin: '',
            appointments: [],
        };
        this.handleChangeSearchVin = this.handleChangeSearchVin.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    };

    async handleChangeSearchVin(event) {
        const value = event.target.value;
        this.setState({search_vin: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        const url = 'http://localhost:8080/api/appointments/';
        const response = await fetch(url);

        if(response.ok) {
            const data = await response.json();
            const appointmentList = data.appointments
            const appointments = [];
            for (let appointment of appointmentList) {
                if(appointment.automobile_vin.includes(this.state.search_vin)) {
                    appointments.push(appointment)
                    console.log(appointments)
                }
            }
            this.setState({ appointments: appointments })
        }
    
    } 

    render () {
        return (
            <div>
                <div>
                    <form onSubmit={this.handleSubmit}>
                        <div className="row justify-content-center mt-3">
                            <div className="input-group mb-3 mt-3">
                                <input className="form-control" onChange={this.handleChangeSearchVin} type="text" id="search" value={this.state.search_vin} placeholder="Search vin" name="search_vin" />
                            </div>
                            <div className="input-group-append">
                                <button className="btn btn-outline-success">Search</button>
                            </div>    
                        </div>
                    </form>
                </div>
                <h1>Service Appointment History</h1>
                <table className="table table-striped table-hover">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Customer</th>
              <th>Date and Time</th>
              <th>Reason</th>
              <th>Technician</th>
              <th>VIP</th>
            </tr>
          </thead>
          <tbody>
            {this.state.appointments.map(appointment => {
                let dateTime = Date.parse(appointment.date_time)
                const newDate = new Date(dateTime)
                let vipStatus = 'No';
                let finished = '';
                if (appointment.vip === true) {
                    vipStatus = 'Yes';
                }
                if (appointment.is_finished === false) {
                    finished = 'd-none'
                }

                return (
                    <tr key={appointment.id}>
                        <td className={finished}>{ appointment.automobile_vin }</td>
                        <td className={finished}>{ appointment.customer }</td>
                        <td className={finished}>{ newDate.toLocaleString( 'en-US', {month:'long', day:'numeric', year:'numeric', minute:'numeric'}) }</td>
                        <td className={finished}>{ appointment.reason }</td>
                        <td className={finished}>{ appointment.technician.name }</td>
                        <td className={finished}>{ vipStatus }</td>
                    </tr>
                )
            })}
          </tbody>
        </table>
        </div>
        );
    }
    }




export default ServiceHistory;