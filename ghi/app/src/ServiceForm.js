import React from 'react';

class ServiceForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            automobile_vin: "",
            customer: "",
            date_time: "",
            reason: "",
            technician: "",
            technicians: []
        };
        this.handleChangeAutomobileVin = this.handleChangeAutomobileVin.bind(this);
        this.handleChangeCustomer = this.handleChangeCustomer.bind(this);
        this.handleChangeDateTime = this.handleChangeDateTime.bind(this);
        this.handleChangeReason = this.handleChangeReason.bind(this);
        this.handleChangeTechnician = this.handleChangeTechnician.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8080/api/technicians/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ technicians: data.technicians });
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.technicians

        const serviceUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(serviceUrl, fetchConfig);
        if (response.ok) {
            const newAppt = await response.json();
            console.log(newAppt);
            const cleared ={
                automobile_vin: "",
                customer: "",
                date_time: "",
                reason: "",
                technician: "",
            };
            this.setState(cleared);
        }
    }

    handleChangeAutomobileVin(event) {
        const value = event.target.value;
        this.setState({ automobile_vin: value });
    }

    handleChangeCustomer(event) {
        const value = event.target.value;
        this.setState({ customer: value });
    }

    handleChangeDateTime(event) {
        const value = event.target.value;
        this.setState({ date_time: value });
    }

    handleChangeReason(event) {
        const value = event.target.value;
        this.setState({ reason: value });
    }

    handleChangeTechnician(event) {
        const value = event.target.value;
        this.setState({ technician: value })
    }

    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create an Appointment</h1>
            <form onSubmit={this.handleSubmit} id="create-technician-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeAutomobileVin} value={this.state.automobile_vin} placeholder="Automobile vin" required type="text" name="automobile_vin" id="automobile_vin" className="form-control" />
                <label htmlFor="name">VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeCustomer} value={this.state.customer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
                <label htmlFor="customer">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeDateTime} value={this.state.date_time} placeholder="Date and time" type="datetime-local" name="date_time" id="date_time" className="form-control" />
                <label htmlFor="dateTime">Date and Time</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeReason} value={this.state.reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                <label htmlFor="reason">Reason for Appointment</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleChangeTechnician} value={this.state.technician} required name="technician" id="technician" className="form-select">
                  <option value="">Choose a technician</option>
                  {this.state.technicians.map(technician => {
                    return (
                      <option key={technician.id} value={technician.id}>{technician.name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        );
    }

}

export default ServiceForm;