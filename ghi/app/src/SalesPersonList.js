import React from "react";
import { Link } from "react-router-dom";

function SalesPersonList(props) {

    return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Sales person</th>
              <th>Employer ID</th>
            </tr>
          </thead>
          <tbody>
            {props.sales_persons.map(sales_person => {
              return (
                <tr key={sales_person.id}>
                  <td>{sales_person.name}</td>
                  <td>{sales_person.employer_id}</td>
              </tr>
              );
            })}
            <tr>
                <td>
                    <Link to='/sales_person/new'>Add a sales person</Link>
                </td>
                <td>
                    <Link to='/sales_person/delete'>Delete a sales person</Link>
                </td>
            </tr>
          </tbody>
        </table>
    );
}

export default SalesPersonList; 