import React from 'react';
import { Link } from "react-router-dom";


function VehicleModelsList(props) {
    return(
        <table className="table table-striped table-hover">
          <thead>
            <tr>
              <th>Name</th>
              <th>Manufactors</th>
              <th>Pictures</th>
            </tr>
          </thead>
          <tbody>
            {props.models.map(model => {
              return (
                <tr key={model.id}>
                  <td>{ model.name }</td>
                  <td>{ model.manufacturer.name }</td>
                  <td><img src={model.picture_url} height='143px' width='254px' /></td>
                </tr>
              );
            })}
            <tr>
              <td>
                <Link to='/vehicleModels/new'>Add Vehicle Model</Link>
              </td>
              <td>
                <Link to='/vehicleModels/delete'>Delete Vehicle Model</Link>
              </td>
            </tr>
          </tbody>
        </table>
    )
};

export default VehicleModelsList;
