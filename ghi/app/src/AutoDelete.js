import React from 'react';

class AutoDelete extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      auto: "",
      autos: []
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleAutoSelection = this.handleAutoSelection.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/automobiles/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ autos: data.autos });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};


    const locationUrl = `http://localhost:8100/api/automobiles/${this.state.auto}`;
    const fetchConfig = {
      method: "delete",
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newAuto = await response.json();
      console.log(newAuto)

      let remainingAutos = []
      for (let i = 0; i < this.state.autos.length; i++) {
        let currentAuto = this.state.autos[i]
        if (parseInt(this.state.auto) !== currentAuto.id) {
          remainingAutos.push(currentAuto)
        }
      }

      this.setState({
        auto: '',
        autos: remainingAutos
      })
    }
  }

  handleAutoSelection(event) {
    const value = event.target.value;
    this.setState({ auto: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Select An Automobile To Remove</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
              <div className="mb-3">
                <select onChange={this.handleAutoSelection} value={this.state.auto} required name="auto" id="auto" className="form-select">
                  <option value="">Choose Automobile Vin</option>
                  {this.state.autos.map(auto => {
                    return (
                      <option key={auto.id} value={auto.vin}>{auto.vin}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Remove</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AutoDelete;
