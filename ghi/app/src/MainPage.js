import {link} from 'react-router-dom'

function MainPage(props) {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
        <div className="container">
          <h2 className="display-5 fw-bold">Greate Vehicles</h2>
          <div className="row">
            {props.autos.map(auto => {
              return (
                <div key={auto.id} className="col">
                  <div className="card mb-3 shadow">
                    <img src={auto.model.picture_url} className="card-img-top" />
                    <div className="card-body">
                      <h5 className="card-title">{auto.year} {auto.model.manufacturer.name} {auto.model.name}</h5>
                      <h6 className="card-subtitle mb-2 text-muted">
                        color: {auto.color}
                      </h6>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}


export default MainPage;

//This is another test