import React from 'react';

class DeleteSalesRecordForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sales_records: []
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSalesRecordSelection = this.handleSalesRecordSelection.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8090/api/autosales/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ sales_records: data.sales });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();

    const salesRecordUrl = `http://localhost:8090/api/customers/${this.state.sales_record}`;
    const fetchConfig = {
      method: "delete",
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(salesRecordUrl, fetchConfig);
    if (response.ok) {
      const newSalesRecord = await response.json();
      console.log(newSalesRecord)

      // Remove hat from state without making new API call
      let remainingSalesRecords = []
      for (let i = 0; i < this.state.sales_records.length; i++) {
        let currentSales_records = this.state.sales_records[i]
        if (parseInt(this.state.sales_record) !== currentSales_records.id) {
            remainingSalesRecords.push(currentSales_records)
        }
      }

      this.setState({
        sales_record: '',
        sales_records: remainingSalesRecords
      })
    }
  }

  handleSalesRecordSelection(event) {
    const value = event.target.value;
    this.setState({ sales_record: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Select Customer To Remove</h1>
            <form onSubmit={this.handleSubmit} id="delete-sales-person-form">
              <div className="mb-3">
                <select onChange={this.handleSalesRecordSelection} value={this.state.sales_record} required name="sales_record" id="sales_record" className="form-select">
                  <option value="">Choose a sales record</option>
                  {this.state.sales_records.map(sales_record => {
                    return (
                      <option key={sales_record.id} value={sales_record.id}>
                          sales person: {sales_record.sales_person.name} / customer: {sales_record.potential_customer.name} / 
                          vehicle: {sales_record.automobile.vin}
                          </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Remove</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default DeleteSalesRecordForm;