import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import AutoForm from './AutoForm';
import VheicleModelForm from './VheicleModelForm';
import VehicleModelsList from './VheicleModelsList';
import AutomobilesList from './AutomobilesList';
import AddSalesPersonForm from './AddSalesPersonForm';
import AddPotentialCustomerForm from './AddPotentialCustomerForm';
import SaleRecordForm from './SaleRecordForm';
import TechnicianForm from './TechnicianForm';
import TechList from './TechList';
import ServiceForm from './ServiceForm';
import SalesPersonHistory from './SalesPersonHistory';
import SalesPersonList from './SalesPersonList';
import DeleteSalesPersonForm from './DeleteSalesPersonForm';
import CustomerList from './CustomerList';
import DeleteCustomerForm from './DeleteCustomerForm';
import DeleteSalesRecordForm from './DeleteSalesRecordForm';
import ServiceList from './ServiceList';
import ServiceHistory from './ServiceHistory';
import ManufacturerDelete from './ManufacturerDelete';
import AutoDelete from './AutoDelete';
import VehicleDelete from './VehicleDelete';
import TechnicianDelete from './TechDelete';


function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage autos={props.autos} />} />
          <Route path="manufacturers">
            <Route index element={<ManufacturersList manufacturers={props.manufacturers}/>} />
            <Route path="new" element={<ManufacturerForm />}/>
            <Route path="delete" element={< ManufacturerDelete/>} />
          </Route>
          <Route path='vehicleModels'>
            <Route index element={<VehicleModelsList models={props.models} />} />
            <Route path='new' element={<VheicleModelForm />} />
            <Route path='delete' element={< VehicleDelete />} />
          </Route>
          <Route path='automobiles'>
            <Route index element={<AutomobilesList autos={props.autos} />} />
            <Route path="new" element={< AutoForm />} />
            <Route path="delete" element={< AutoDelete />} />
          </Route>
          <Route path='sales_person'>
            <Route index element={<SalesPersonList sales_persons={props.sales_person} />} />
            <Route path='new' element={<AddSalesPersonForm />} />
            <Route path='delete' element={<DeleteSalesPersonForm />} />
          </Route>
          <Route path='customer'>
            <Route index element={<CustomerList customers={props.customer} />} />
            <Route path='new' element={<AddPotentialCustomerForm />} />
            <Route path='delete' element={<DeleteCustomerForm />} />
          </Route>
          <Route path='sales'>
            <Route index element={<SalesPersonHistory />} />
            <Route path='new' element={<SaleRecordForm />} />
            <Route path='delete' element={<DeleteSalesRecordForm />} />
          </Route>
          <Route path="servicehistory" element={< ServiceHistory/>} />
          <Route path="appointments">
            <Route path='new' element={< ServiceForm />} />
            <Route path='' element={< ServiceList />} />
          </Route>
          <Route path="technicians">
            <Route path='' element={< TechList  technicians={props.technician}  />} />
            <Route path="new" element={< TechnicianForm />} />
            <Route path="delete" element={< TechnicianDelete />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}


export default App;

