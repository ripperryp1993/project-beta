import React from 'react';

class DeleteCustomerForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      customers: []
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCustomerSelection = this.handleCustomerSelection.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8090/api/customers/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ customers: data.potential_customer });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();

    const customerUrl = `http://localhost:8090/api/customers/${this.state.customer}`;
    const fetchConfig = {
      method: "delete",
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(customerUrl, fetchConfig);
    if (response.ok) {
      const newCustomer = await response.json();
      console.log(newCustomer)

      // Remove hat from state without making new API call
      let remainingCustomers = []
      for (let i = 0; i < this.state.customers.length; i++) {
        let currentCustomer = this.state.customers[i]
        if (parseInt(this.state.customer) !== currentCustomer.id) {
            remainingCustomers.push(currentCustomer)
        }
      }

      this.setState({
        customer: '',
        customers: remainingCustomers
      })
    }
  }

  handleCustomerSelection(event) {
    const value = event.target.value;
    this.setState({ customer: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Select Customer To Remove</h1>
            <form onSubmit={this.handleSubmit} id="delete-sales-person-form">
              <div className="mb-3">
                <select onChange={this.handleCustomerSelection} value={this.state.customer} required name="customer" id="customer" className="form-select">
                  <option value="">Choose a customer</option>
                  {this.state.customers.map(customer => {
                    return (
                      <option key={customer.id} value={customer.id}>{customer.name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Remove</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default DeleteCustomerForm;