import React from "react";
import { Link } from "react-router-dom";

function TechList(props) {

    return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Technician Name</th>
              <th>Employee Number</th>
            </tr>
          </thead>
          <tbody>
            {props.technicians.map(technician => {
              return (
                <tr key={technician.id}>
                  <td>{technician.name}</td>
                  <td>{technician.employee_number}</td>
              </tr>
              );
            })}
            <tr>
                <td>
                    <Link to='/technicians/new'>Add a Technician</Link>
                </td>
                <td>
                    <Link to='/technicians/delete'>Delete a Technician</Link>
                </td>
            </tr>
          </tbody>
        </table>
    );
}

export default TechList; 