import React from 'react';

class SaleRecordForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        automobile: '',
        sales_person: '',
        potential_customer: '',
        sale_price: '',
        autos: [],
        sales_persons: [],
        customers: []
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeAuto = this.handleChangeAuto.bind(this);
    this.handleChangeSalesPerson = this.handleChangeSalesPerson.bind(this);
    this.handleChangeCustomer = this.handleChangeCustomer.bind(this);
    this.handleChangeSalePrice = this.handleChangeSalePrice.bind(this);
  }

  async componentDidMount() {
    const autoUrl = 'http://localhost:8100/api/automobiles/';

    const autoresponse = await fetch(autoUrl);

    if (autoresponse.ok) {
      const autodata = await autoresponse.json();
      this.setState({ autos: autodata.autos });
    }

    const salesPersonUrl = 'http://localhost:8090/api/sales_person/';
    const salesPersonResponse = await fetch(salesPersonUrl);
    if(salesPersonResponse.ok) {
        const salesPersonData = await salesPersonResponse.json();
        this.setState({sales_persons: salesPersonData.sales_person})
    }

    const customerUrl = 'http://localhost:8090/api/customers/';
    const customerResponse = await fetch(customerUrl);
    if(customerResponse.ok) {
        const customerData = await customerResponse.json();
        this.setState({customers: customerData.potential_customer})
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.autos;
    delete data.sales_persons;
    delete data.customers;


    const autoSalesUrl = 'http://localhost:8090/api/autosales/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(autoSalesUrl, fetchConfig);
    if (response.ok) {
      const newAutoSale = await response.json();
      console.log(newAutoSale);
      this.setState({
        automobile: '',
        sales_person: '',
        potential_customer: '',
        sale_price: '',
      });
    }
  }

  handleChangeAuto(event) {
    const value = event.target.value;
    this.setState({ automobile: value });
  }

  handleChangeSalesPerson(event) {
    const value = event.target.value;
    this.setState({ sales_person: value });
  }

  handleChangeCustomer(event) {
      const value = event.target.value;
      this.setState({ potential_customer: value })
  }

  handleChangeSalePrice(event) {
    const value = event.target.value;
    this.setState({ sale_price: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Record a new sale</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
            <div className="mb-3">
                <select onChange={this.handleChangeAuto} value={this.state.automobile} required name="auto" id="auto" className="form-select">
                  <option value="">Choose an automobile</option>
                  {this.state.autos.map(auto => {
                    return (
                      <option key={auto.href} value={auto.href}>{auto.color} {auto.year} {auto.model.manufacturer.name} {auto.model.name}</option>
                    )
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select onChange={this.handleChangeSalesPerson} value={this.state.sales_person} required name="sales_person" id="sales_person" className="form-select">
                  <option value="">Choose a sales person</option>
                  {this.state.sales_persons.map(sales_person => {
                    return (
                      <option key={sales_person.name} value={sales_person.name}>{sales_person.name}</option>
                    )
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select onChange={this.handleChangeCustomer} value={this.state.potential_customer} required name="customer" id="customer" className="form-select">
                  <option value="">Choose a customer</option>
                  {this.state.customers.map(customer => {
                    return (
                      <option key={customer.name} value={customer.name}>{customer.name}</option>
                    )
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                  <input onChange={this.handleChangeSalePrice} value={this.state.sale_price} placeholder="Sale price" required type="number" name="sale_price" id="sale_price" className="form-control" />
                  <label htmlFor="sale_price">Sale price</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default SaleRecordForm;