import React from 'react';

class VehicleDelete extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      model: "",
      models: []
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleVehicleSelection = this.handleVehicleSelection.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/models/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ models: data.models });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    // delete data.models

    const locationUrl = `http://localhost:8100/api/models/${this.state.model}`;
    const fetchConfig = {
      method: "delete",
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newVehicle = await response.json();
      console.log(newVehicle)

      let remainingVehicles = []
      for (let i = 0; i < this.state.models.length; i++) {
        let currentVehicle = this.state.models[i]
        if (parseInt(this.state.hat) !== currentVehicle.id) {
          remainingVehicles.push(currentVehicle)
        }
      }

      this.setState({
        model: '',
        models: remainingVehicles
      })
    }
  }

  handleVehicleSelection(event) {
    const value = event.target.value;
    this.setState({ model: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Select vehicle To Remove</h1>
            <form onSubmit={this.handleSubmit} id="create-vehicle-form">
              <div className="mb-3">
                <select onChange={this.handleVehicleSelection} value={this.state.model} required name="model" id="model" className="form-select">
                  <option value="">Choose a vehicle</option>
                  {this.state.models.map(model => {
                    return (
                      <option key={model.id} value={model.id}>{model.name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Remove</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default VehicleDelete;