import React from 'react';
import { Link } from "react-router-dom";

class SalesPersonHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        sales_person: '',
        sales: [],
        sales_persons: [],
    };

    //this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeSalesPerson = this.handleChangeSalesPerson.bind(this);
  }

  async componentDidMount() {
    

    const salesPersonUrl = 'http://localhost:8090/api/sales_person/';
    const salesPersonResponse = await fetch(salesPersonUrl);
    if(salesPersonResponse.ok) {
        const salesPersonData = await salesPersonResponse.json();
        this.setState({sales_persons: salesPersonData.sales_person})
    }

    

    // const customerUrl = 'http://localhost:8090/api/customers/';
    // const customerResponse = await fetch(customerUrl);
    // if(customerResponse.ok) {
    //     const customerData = await customerResponse.json();
    //     this.setState({customers: customerData.potential_customer})
    // }
  }

//   async handleSubmit(event) {
//     event.preventDefault();
//     const data = {...this.state};
//     delete data.autos;
//     delete data.sales_persons;
//     delete data.customers;


//     const autoSalesUrl = 'http://localhost:8090/api/autosales/';
//     const fetchConfig = {
//       method: "post",
//       body: JSON.stringify(data),
//       headers: {
//         'Content-Type': 'application/json',
//       },
//     };
//     const response = await fetch(autoSalesUrl, fetchConfig);
//     if (response.ok) {
//       const newAutoSale = await response.json();
//       console.log(newAutoSale);
//       this.setState({
//         automobile: '',
//         sales_person: '',
//         potential_customer: '',
//         sale_price: '',
//       });
//     }
//   }


  async handleChangeSalesPerson(event) {
    const value = event.target.value;
    this.setState({ sales_person: value });
    
    const autoSalesUrl = 'http://localhost:8090/api/autosales/';
    const autoSalesresponse = await fetch(autoSalesUrl);

    if (autoSalesresponse.ok) {
      const autoSalesData = await autoSalesresponse.json();
      const sales = autoSalesData.sales;
      console.log(sales)
      const newSales = [];
      for (const sale of sales) {
          if (sale.sales_person.name === this.state.sales_person) {
            newSales.push(sale);
          };
      };
      this.setState({sales: newSales});
    };
  };




  


  render() {
    return (
      <div>
            <h1>Sales person history</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
              <div className="mb-3">
                <select onChange={this.handleChangeSalesPerson} value={this.state.sales_person} required name="sales_person" id="sales_person" className="form-select">
                  <option value="">Choose a sales person</option>
                  {this.state.sales_persons.map(sales_person => {
                    return (
                      <option key={sales_person.name} value={sales_person.name}>{sales_person.name}</option>
                    )
                  })}
                </select>
              </div>
            </form>
            <table className="table table-striped table-hover">
              <thead>
                <tr>
                  <th>Sales person</th>
                  <th>Customer</th>
                  <th>Vin</th>
                  <th>Sale price</th>
                </tr>
              </thead>
              <tbody>
                {this.state.sales.map(sale => {
                  return (
                    <tr key={sale.id}>
                      <td>{ sale.sales_person.name }</td>
                      <td>{ sale.potential_customer.name }</td>
                      <td>{ sale.automobile.vin }</td>
                      <td>{sale.sale_price}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <p>
              <Link to='/sales/new'>Add a sales record</Link>
            </p>
            <Link to='/sales/delete'>Delete a sales history</Link> 
      </div>
    );
  }
}

export default SalesPersonHistory;