import React from 'react';

class AddSalesPersonForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            employer_id:'',
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmployerIDChange = this.handleEmployerIDChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    };

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        const salesPersonUrl = 'http://localhost:8090/api/sales_person/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salesPersonUrl, fetchConfig);
        if (response.ok) {
            const newsalesPerson = await response.json();
            console.log(newsalesPerson);
            const cleared = {
                name: '',
                employer_id: '',
              };
              this.setState(cleared);
        }
    };

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value});
    };

    handleEmployerIDChange(event) {
        const value = event.target.value;
        this.setState({employer_id: value});
    };



    render() {
        return(
            <div className="my-5 container">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>New sales person</h1>
                            <form onSubmit={this.handleSubmit} id="create-model-form">
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text"  name="name" id="name" className="form-control"/>
                                    <label htmlFor="name">Name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleEmployerIDChange} value={this.state.employer_id} placeholder="Employer ID" required type="text" name="employer_id" id="employer_id" className="form-control"/>
                                    <label htmlFor="employer_id">Employer ID</label>
                                </div>
                                <button className="btn btn-primary">Add</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    };

};

export default AddSalesPersonForm;